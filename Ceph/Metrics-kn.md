```sh
kubectl edit configmap/config-observability -n knative-serving
kubectl edit configmap/config-observability -n knative-eventing
```

```yaml
data:
  backend: zipkin
  metrics.backend-destination: opencensus
  metrics.opencensus-address: my-collector-collector.observability.svc:55678
  metrics.request-metrics-backend-destination: opencensus
```
```sh
kubectl edit configmap/config-tracing -n knative-serving
kubectl edit configmap/config-tracing -n knative-eventing
```

```yaml
# ...
data:
  backend: zipkin
  debug: "true"
  zipkin-endpoint: http://my-collector-collector.observability.svc:9411/api/v2/spans
# ...

```


```sh
kubectl edit configmap/config-logging -n knative-serving
kubectl edit configmap/config-logging -n knative-eventing
```


```yaml
data:
  zap-logger-config: |
    {
      "level": "debug",
      "development": false,
      "outputPaths": ["stdout"],
      "errorOutputPaths": ["stderr"],
      "encoding": "json",
      "encoderConfig": {
        "timeKey": "timestamp",
        "levelKey": "severity",
        "nameKey": "logger",
        "callerKey": "caller",
        "messageKey": "message",
        "stacktraceKey": "stacktrace",
        "lineEnding": "",
        "levelEncoder": "",
        "timeEncoder": "iso8601",
        "durationEncoder": "",
        "callerEncoder": ""
      }
    }



```