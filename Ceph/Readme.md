TASK [enable red hat ceph storage tools repository] ***************************************************************************************************************************
task path: /home/vagrant/cephadm-ansible/cephadm-preflight.yml:44
skipping: [localhost] => changed=false 
  false_condition: ansible_facts['distribution'] == 'CentOS' or ansible_facts['distribution'] == 'RedHat'
  skip_reason: Conditional result was False

  Verifying ssh connectivity ...
Adding key to vagrant@localhost authorized_keys...
key already in vagrant@localhost authorized_keys...
Verifying podman|docker is present...
Verifying lvm2 is present...
Verifying time synchronization is in place...
Unit chronyd.service is enabled and running
Repeating the final host check...
podman (/bin/podman) version 4.4.1 is present
systemctl is present
lvcreate is present
Unit chronyd.service is enabled and running
Host looks OK
Cluster fsid: 200a52dc-733e-11ee-bf6b-000c296398ec
Verifying IP 10.100.0.21 port 3300 ...
Verifying IP 10.100.0.21 port 6789 ...
Mon IP `10.100.0.21` is in CIDR network `10.100.0.0/24`
Mon IP `10.100.0.21` is in CIDR network `10.100.0.0/24`
Internal network (--cluster-network) has not been provided, OSD replication will default to the public_network
Pulling container image quay.io/ceph/ceph:v17...
Ceph version: ceph version 17.2.6 (d7ff0d10654d2280e08f1ab989c7cdf3064446a5) quincy (stable)
Extracting ceph user uid/gid from container image...
Creating initial keys...
Creating initial monmap...
Creating mon...
Waiting for mon to start...
Waiting for mon...
mon is available
Assimilating anything we can from ceph.conf...
Generating new minimal ceph.conf...
Restarting the monitor...
Setting mon public_network to 10.100.0.0/24
Wrote config to /etc/ceph/ceph.conf
Wrote keyring to /etc/ceph/ceph.client.admin.keyring
Creating mgr...
Verifying port 9283 ...
Waiting for mgr to start...
Waiting for mgr...
mgr not available, waiting (1/15)...
mgr not available, waiting (2/15)...
mgr not available, waiting (3/15)...
mgr not available, waiting (4/15)...
mgr is available
Enabling cephadm module...
Waiting for the mgr to restart...
Waiting for mgr epoch 5...
mgr epoch 5 is available
Setting orchestrator backend to cephadm...
Using provided ssh keys...
Adding key to vagrant@localhost authorized_keys...
key already in vagrant@localhost authorized_keys...
Adding host ceph1...
Deploying mon service with default placement...
Deploying mgr service with default placement...
Deploying crash service with default placement...
Deploying ceph-exporter service with default placement...
Deploying prometheus service with default placement...
Deploying grafana service with default placement...
Deploying node-exporter service with default placement...
Deploying alertmanager service with default placement...
Enabling the dashboard module...
Waiting for the mgr to restart...
Waiting for mgr epoch 9...
mgr epoch 9 is available
Generating a dashboard self-signed certificate...
Creating initial admin user...
Fetching dashboard port number...
Ceph Dashboard is now available at:

             URL: https://ceph1:8443/
            User: admin
        Password: znruuz2gov

Enabling client.admin keyring and conf on hosts with "admin" label
Applying cluster-spec.yaml to cluster
Added ssh key to host ceph2 at address 10.100.0.22
Added ssh key to host ceph3 at address 10.100.0.23
Added ssh key to host ceph4 at address 10.100.0.24
Added ssh key to host ceph5 at address 10.100.0.25
Added ssh key to host ceph6 at address 10.100.0.26
Added ssh key to host ceph7 at address 10.100.0.27
Non-zero exit code 22 from /bin/podman run --rm --ipc=host --stop-signal=SIGTERM --net=host --entrypoint /usr/bin/ceph --init -e CONTAINER_IMAGE=quay.io/ceph/ceph:v17 -e NODE_NAME=ceph1 -e CEPH_USE_RANDOM_NONCE=1 -v /var/log/ceph/200a52dc-733e-11ee-bf6b-000c296398ec:/var/log/ceph:z -v /tmp/ceph-tmp_8vegj0v:/etc/ceph/ceph.client.admin.keyring:z -v /tmp/ceph-tmpfh98b8rg:/etc/ceph/ceph.conf:z -v /home/vagrant/cluster-spec.yaml:/tmp/spec.yml:ro quay.io/ceph/ceph:v17 orch apply -i /tmp/spec.yml
/usr/bin/ceph: stderr Error EINVAL: check-host failed:
/usr/bin/ceph: stderr systemctl is present
/usr/bin/ceph: stderr Unit chronyd.service is enabled and running
/usr/bin/ceph: stderr Hostname "ceph2" matches what is expected.
/usr/bin/ceph: stderr ERROR: No container engine binary found (podman or docker). Try run `apt/dnf/yum/zypper install <container engine>`
/usr/bin/ceph: stderr ERROR: lvcreate binary does not appear to be installed

Applying cluster-spec.yaml to cluster failed!

Saving cluster configuration to /var/lib/ceph/200a52dc-733e-11ee-bf6b-000c296398ec/config directory
Enabling autotune for osd_memory_target
You can access the Ceph CLI as following in case of multi-cluster or non-default config:

        sudo /sbin/cephadm shell --fsid 200a52dc-733e-11ee-bf6b-000c296398ec -c /etc/ceph/ceph.conf -k /etc/ceph/ceph.client.admin.keyring

Or, if you are only running a single cluster on this host:

        sudo /sbin/cephadm shell

Please consider enabling telemetry to help improve Ceph:

        ceph telemetry on

For more information see:

        https://docs.ceph.com/docs/master/mgr/telemetry/

Bootstrap complete.



https://docs.ceph.com/en/latest/rbd/rbd-kubernetes/#create-a-pool




```mermaid

flowchart TD

    c1["ceph1
    10.100.0.21
    _admin
    mgr
    mon
    osd"]

    c2["ceph2
    10.100.0.22
    mon
    osd"]

    c3["ceph3
    10.100.0.23
    mds
    osd
    rgw"]

    c4["ceph4
    10.100.0.24
    mgr
    mon
    osd
    "]

    c5["ceph5
    10.100.0.25    
    mon
    osd
    "]

    c6["ceph6
    10.100.0.26
    mds    
    osd
    rgw
    "]

    c7["ceph7
    10.100.0.27
    mon
    "]

    KM["K8MasterNode
    10.100.0.11
    Kubernetes Master Node
    "]

    KN1["K8Node1
    10.100.0.12
    Kubernetes Worker Node 1
    "]

    KN2["K8Node2
    10.100.0.13
    Kubernetes Worker Node 2
    "]

    net{{10.100.0.0/24}}

    s4[(/dev/sdb)]
    s5[(/dev/sdb)]
    s6[(/dev/sdb)]
    s7[(/dev/sdb)]

    net ---|eth1| c1
    net ---|eth1| c2
    net ---|eth1| c3
    net ----|eth1| c4
    net ----|eth1| c5
    net ----|eth1| c6
    net ----|eth1| c7

    net ---|eth1| KM
    net ---|eth1| KN1
    net ---|eth1| KN2

    s4 --- c4
    s5 --- c5
    s6 --- c6
    s7 --- c7
```
