sudo dnf -y install python3.9
python3 -m ensurepip --upgrade

pip3 install -r https://raw.githubusercontent.com/kubernetes-sigs/kubespray/v2.22.1/requirements-2.12.txt

sudo mkdir -p /etc/ansible/
sudo cp /vagrant/hosts.ini /etc/ansible/hosts # read for all

rm -rf /home/vagrant/.ssh/known_hosts

export ANSIBLE_HOST_KEY_CHECKING=False; # You may want to put this here '/etc/profile.d/'
ansible all -m ping -v

cd /vagrant/

ansible-playbook ceph-playbook.yaml

if [ ! -f "/usr/bin/ceph" ]; then
    echo "ceph not exists."
    sudo cephadm bootstrap \     
    --ssh-user=vagrant \ 
    --mon-ip 10.100.0.21 \
    --apply-spec /home/vagrant/cluster-spec.yaml \ 
    --ssh-private-key /home/vagrant/.ssh/id_rsa \ 
    --ssh-public-key /home/vagrant/.ssh/id_rsa.pub
    
    ansible-playbook ceph-reset-admin-passwd.yaml
fi


ansible-playbook kubespray-prepare.yaml

cd /home/vagrant/kubespray/

cp /etc/ansible/hosts /home/vagrant/kubespray/inventory/mycluster/ansible_inventory.ini
ansible-playbook -i inventory/mycluster/ansible_inventory.ini --become --become-user=root cluster.yml

# ansible-playbook -i /etc/ansible/hosts --become --become-user=root cluster.yml  # since 2020 i don't know why this way will skip config

cd /vagrant
ansible-playbook kubespray-post.yaml

ssh 10.100.0.10 kubectl get all --all-namespaces


# get kubectl config and update ip
master_node_ip=`ssh 10.100.0.10 "ip -f inet addr show eth0 | grep -oP 'inet (\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})' | cut -d ' ' -f 2"`

echo "Use commands shown below to copy kubeconfig and run kubectl proxy to proxy"
echo "scp vagrant@${master_node_ip}:/home/vagrant/public_kube public_kube"
echo "kubectl --kubeconfig public_kube proxy"
echo "kuberntes dashboard address"

echo "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"

echo "Login token"
echo "kubectl --kubeconfig public_kube -n kubernetes-dashboard create token admin-user"

# When everything started
ansible-playbook ceph-and-kubernetes.yaml 

kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml
ansible-playbook install_helm_apps.yaml 

sudo ceph config rm mon mon/public_network
sudo ceph config set mon public_network  192.168.3.0/24,10.100.0.0/24
sudo ceph mgr module disable dashboard
sudo ceph mgr module enable dashboard
sudo ceph config get mon
