ceph osd pool create cephfs_data
ceph osd pool create cephfs_metadata

ceph fs new cephfs cephfs_metadata cephfs_data
ceph mds stat


fsid=$(ceph mon dump | grep fsid | cut -d ' ' -f 2)
echo "fsid: $fsid"

szymon_key=$(ceph fs authorize cephfs client.szymon / rw | grep -oP '(key = \K.*)')
echo "user key: $szymon_key"

mkdir -p /mnt/mycephfs

echo "mount -t ceph szymon@`echo $fsid`.cephfs=/ -o mon_addr=10.100.0.21:6789,secret=`echo $szymon_key` /mnt/mycephfs"
mount -t ceph "szymon@${fsid}.cephfs=/" -o "mon_addr=10.100.0.21:6789,secret=${szymon_key}" /mnt/mycephfs


mkdir -p /mnt/mycephfs/cephfs_nfs_folder
# trzeba mieć uruchomiony serwis
# potem z gui dodanie nfs-a


# Na maszynie jakieś
mkdir -p /mnt/nfs/
mount -t nfs -o port=2049 10.100.0.23:/cephfs_nfs /mnt/nfs/

# dd if=/dev/zero of=/mnt/mycephfs/`hostname` bs=1M count=1000
# dd if=/dev/zero of=/mnt/nfs/`hostname` bs=1M count=1000
