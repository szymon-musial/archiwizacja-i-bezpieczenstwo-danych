helm repo add istio https://istio-release.storage.googleapis.com/charts


helm upgrade --install istio-base istio/base --version 1.18.1 --namespace istio-system  --create-namespace --set defaultRevision=1.18.1
helm upgrade --install istiod istio/istiod --version 1.18.1 --namespace istio-system -f Deployments/Helms/istio-istiod-values.yaml --wait

kubectl label namespace default istio-injection=enabled 

helm upgrade --install istio-ingressgateway istio/gateway --version 1.18.1 --namespace istio-system --create-namespace -f Deployments/Helms/istio-gateway-values.yaml

kubectl apply -f Deployments/istio-metrics-scrape.yaml

kubectl apply -f Deployments/tempo-query-jeager-api-ui.yaml


helm upgrade --install kiali-operator kiali/kiali-operator --version 1.72.0 --set cr.create=true --set cr.namespace=istio-system --namespace istio-system --create-namespace
kubectl apply -f Deployments/kiali-server-via-operator.yaml

# sample app
#
# za dużo bierze cpu
# kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/v0.8.0/release/kubernetes-manifests.yaml
