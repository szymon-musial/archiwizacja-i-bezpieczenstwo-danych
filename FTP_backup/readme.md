# 10 archiwizacja sprawozdanie

Szymon Musiał, Szymon Sumara

---

Całe sprawozdanie było realizowane na systemie `Debian 12`

## Instalacja i konfiguracja vsftpd

Instaluje vsftpd
`sudo apt install vsftpd`

Następnie uruchamiamy serwis
`systemctl enable vsftpd`

Do pliku `/etc/vstpd.conf` należy dokleić następujący fragment konfiguracji:

```
chroot_local_user=YES
allow_writeable_chroot=YES
hide_ids=YES

user_config_dir=/etc/vsftpd_user_conf
guest_enable=YES
virtual_use_local_privs=YES
pam_service_name=vsftpd
nopriv_user=vsftpd
guest_username=vsftpd
```

### Tworzenie wirtualnych użytkowników

Pierwszym krokiem jest dodanie użytkownika, który będzie mieć dostęp do dostępnego katalogu

```
groupadd nogroup
useradd --home-dir /home/vsftpd --gid nogroup -m --shell /bin/false vsftpd
```

Następnie musimy utworzyć bazę danych z wirtualnymi użytkownikami. Pierwszym krokiem jest zainstalowanie wymaganych pakietów.

`dnf install libdb-utils`

Teraz musimy utworzyć plik z wirtualnymi użytkownikami (pierwsza linia to login a następna to hasło)

Zawartość pliku tekstowego

```
backup
1234
```

Tworzymy bazę danych z użytkownikami i zmieniamy uprawnienia do niej.

```
db_load -T -t hash -f vusers.txt vsftpd-virtual-user.db
chmod 600 vsftpd-virtual-user.db
```

Warto utworzyć backup startej konfiguracji pam

```
mkdir /root/backup_vsftpd_pam
cp /etc/pam.d/vsftpd /root/backup_vsftpd_pam/
```

Konfiguracja w pliku `/etc/pam.d/vsftpd` powinna wyglądać następująco.

```
auth	required	pam_userdb.so db=/etc/vsftpd-virtual-user
account	required        pam_userdb.so db=/etc/vsftpd-virtual-user
session	required	pam_loginuid.so
```

Po wykonaniu restartu servisu ( `systemctl restart vsftpd` ) wszystko powinno już działać prawidłowo.

## Testy

Na potrzeby testów zostały przygotowane skrypty

- `backup.sh`, który tworzy backup katalogu `/home/user/wazne-dane` a następnie zapisuje ten utworzony backup w kawałkach po 100Mb na serwerze FTP
- `restore.sh`,który pobiera sekwencyjnie zapisane kawałki backupu a następnie odtwarza katalog `/home/user/wazne-dane` (wszystkie zmiany zostają nadpisane)

Wykonując testy prędkości transferu bez ograniczania otrzymaliśmy wartość ~60MB/s, a ograniczając prędkość transferu do 1Mb/s otrzymaliśmy prędkość około 800 KB/s

## Zawartości sktyptów

#### backup.sh

```
rm -f -R "/tmp/archive"
mkdir "/tmp/archive"
dar -c "/tmp/archive/back" -R "/home/user/wazne-dane/" -s 100M -E "/home/user/scripts/upload.sh"
```

#### upload.sh

```
#!/bin/bash
for file in "/tmp/archive"/*
do
    fileename=$(basename "$file")

    ncftpput -u user_a -p 1234 192.168.1.117  "archive" "$file"
    rm "$file"
done
```

#### restore.sh

```
rm -f -R "/tmp/archive"
mkdir -p "/tmp/archive"
mkdir -p "/home/user/wazne-dane-restored"
dar -x "/tmp/archive/back" -R "/home/user/wazne-dane" --sequential-read -s 100M -E "/home/user/scripts/download.sh" -w
rm -f  "/tmp/.dar_cache"
```

#### download.sh

```
#!/bin/bash
if [[ -f "/tmp/.dar_cache" ]];then
	echo "set in if"
	DAR_SLICE_NUMBER=$(</tmp/.dar_cache)
else
	DAR_SLICE_NUMBER=1

fi

rm  /tmp/archive/*

for file in "/home/user/archive"/*
do
	ncftpget -u user_a -p 1234 192.168.1.117  "/tmp/archive" "archive/back.$DAR_SLICE_NUMBER.dar"
done

echo $(($DAR_SLICE_NUMBER + 1)) > "/tmp/.dar_cache"
```
