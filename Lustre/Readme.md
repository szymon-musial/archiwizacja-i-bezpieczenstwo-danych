## Środowisko

Laboratorium wykonano na wirtualnych maszynach z wykorzystaniem narzędzia „Vagrant”, gdzie wirtualizatorem było „VMware Workstation”
Zadania wykonano na systemie z rodziny RHEL 8.8 gdzie był nim RockyLinux 8.8
Poniżej zaprezentowano konfigurację Maszyn wirtualnych w pliku Vagrant [Vagrantfile](./Vagrantfile)

## Schemat maszyn w klastrze

Poniżej przedstawiono schemat maszyn użytych do ćwiczenia.

```mermaid

flowchart TD
    A["HostA
    MGS, MDT"]
    B["HostB"
    OST]
    C["HostC"
    OST]
    D["HostD
    klient"]

    net{{10.100.0.0/24}}


    Asdb[("MGS
    /dev/sdb
    ")]

    Asdc[("MDT
    /dev/sdc
    ")]

    Bsd[("OST
    /dev/sdb
    /dev/sdc
    ")]

    Csd[("OST
    /dev/sdb
    /dev/sdc
    ")]

    A --- Asdb
    A --- Asdc

    B --- Bsd
    C --- Csd

    net ---|eth1| A
    net ---|eth1| B
    net ---|eth1| C
    net ---|eth1| D
```

## Konfiguracja repozytoriów lustre

Bazując na (mylnej) instrukcji Lustre [https://wiki.lustre.org/Installing_the_Lustre_Software](https://wiki.lustre.org/Installing_the_Lustre_Software) przeprowadzono proces dodawania repozytoriów z paczkami.

Poniżej pokazano fragment z instrukcji pliku `.repo` dla menadżera paczek.

```ini
[lustre-server]
name=lustre-server
baseurl=https://downloads.whamcloud.com/public/lustre/latest-release/el7/server
# exclude=*debuginfo*
gpgcheck=0
```

Można zauważyć że w (starszej) instrukcji paczki są dla systemu bazującego na RHEL 7 oraz bez skonkretyzowanej wersji Lustre. Przechodząc pod link z instrukcji okaże się (nie raz :) że nie działa. Natomiast przechodząc wyżej pod link [https://downloads.whamcloud.com/public/lustre/](https://downloads.whamcloud.com/public/lustre/) można zauważyć różne wersje projektu Lustre. Przechodząc do folderów można dobrać wersję do systemu operacyjnego. W tym przypadku do "minor"-a wersji, a nie "major"-a jak w przypadku RHEL7.

Ostatecznie na dzień realizowania tego ćwiczenia wybrano wersję 2.15.3 na RHEL 8.8 co przełożyło się na przytoczone poniższej definicje.

Dodatkowo jawnie wskazano na włącznie repozytoriów, ponieważ będą one używane.

```ini
[lustre-server]
name=lustre-server
baseurl=https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/server
# exclude=*debuginfo*
gpgcheck=0
enabled=1

[lustre-client]
name=lustre-client
baseurl=https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/client
# exclude=*debuginfo*
gpgcheck=0
enabled=1

[e2fsprogs-wc]
name=e2fsprogs-wc
baseurl=https://downloads.whamcloud.com/public/e2fsprogs/latest/el8
# exclude=*debuginfo*
gpgcheck=0
enabled=1
```

## SELinux

W instrukcji nie zostało nic wspomniane o selinuxie, natomiast z prezentacji podlinkowanej na stronie pochodzącej z dokumentacji Lustre [https://lustre.ornl.gov/lustre101-courses/content/C1/L3/LustreBasicInstall.pdf](https://lustre.ornl.gov/lustre101-courses/content/C1/L3/LustreBasicInstall.pdf) na 8 folii  konieczność wyłączenia SELinux-a.

Kompletne wyłączenie jest możliwe tylko i wyłącznie przez ustawienie stałej `SELINUX=disabled` w pliku `/etc/selinux/config`
Poniżej przedstawiono fragment manuala `man selinux` zawierającego informację o ścieżce do pliku.

`
The /etc/selinux/config configuration file controls whether SELinux is enabled or disabled ...
`

Natomiast w prezentacji wskazano na plik `/etc/sysconfig/selinux`. Po wykonaniu polecenia `vi /etc/sysconfig/selinux` i ustawieniu odpowiedniej flagi bazując ze źródła z prezentacji ustawienie nie miało wpływu na konfigurację.

```bash
[root@HostA vagrant]# ll /etc/sysconfig/selinux 
lrwxrwxrwx. 1 root root 17 May 18 03:21 /etc/sysconfig/selinux -> ../selinux/config
```

Mimo iż symlink powinien zmodyfikować oryginalny plik tak się nie stało i po restarcie tryb pracy był `Enforcing` (najprawdopodobniej w procesie konfiguracji symlink został zerwany) i zmiana w oryginalnym pliki miała wpływ na działanie. [https://serverfault.com/questions/500957/selinux-disabled-but-still-enforcing](https://serverfault.com/questions/500957/selinux-disabled-but-still-enforcing)

Ostatecznie poniża komenda oraz restart rozwiązuje problem.

```sh
sed --in-place "s/SELINUX=*/SELINUX=disabled/" /etc/selinux/config
```

## Paczki systemu

Rozpatrywano instalację `Lustre Servers with ZFS OSD Support`

Następnie według instrukcji zainstalowano repozytorium EPEL oraz pobrano repozytorium systemu plików `ZFS`. Podobnie jak poprzednio trzeba było poszukać poprawny linku do systemu.

Ostateczną instalację można przeprowadzić z poniższego linku:

```sh
dnf -y install https://zfsonlinux.org/epel/zfs-release-2-3.el8.noarch.rpm
```

Kolejnym krokiem według instrukcji było zainstalowanie grupy paczek z jądrem poniższą komendą.

```sh
yum install \
kernel \
kernel-devel \
kernel-headers \
kernel-abi-whitelists \
kernel-tools \
kernel-tools-libs \
kernel-tools-libs-devel
```
Natomiast instrukcja nie przewidziała problemu brakującej paczki `kernel-tools-libs-devel`. Paczka ta znajduje się tzw. PowerRepo. Odpowiednik dla RockyLinux nazywa się `Rocky Linux 8 - PowerTools`

Poniższą komendą można włączyć repozytorium.

```sh
sed --in-place "s/enabled=0/enabled=1/" /etc/yum.repos.d/Rocky-PowerTools.repo
```

Ostatecznie repozytoria przedstawiono poniżej.

```sh
[root@HostA vagrant]# dnf repolist
repo id                                                                repo name
appstream                                                              Rocky Linux 8 - AppStream
baseos                                                                 Rocky Linux 8 - BaseOS
e2fsprogs-wc                                                           e2fsprogs-wc
epel                                                                   Extra Packages for Enterprise Linux 8 - x86_64
extras                                                                 Rocky Linux 8 - Extras
lustre-client                                                          lustre-client
lustre-server                                                          lustre-server
powertools                                                             Rocky Linux 8 - PowerTools
zfs                                                                    ZFS on Linux for EL8 - dkms
```

Teraz zainstalowanie grupy paczek jądra, z ostatnio opisywanej komendy z instrukcji, spowoduje dodanie kolejnego jądra systemu. Poprzez zależność paczek do `kernel-core`. Ponadto doda również moduły z powodu zależności do paczki `kernel-modules`

Następnie wykonano komendy ustawiając id hosta.

```sh
hid=`[ -f /etc/hostid ] && od -An -tx /etc/hostid|sed 's/ //g'`
[ "$hid" = `hostid` ] || genhostid
```

Oraz zrestartowano maszynę aby uruchomić ją na nowym jądrze systemu.

Kolejnym krokiem było zainstalowanie paczek lustre oraz zfs poniższą komendą

```sh
yum --nogpgcheck --enablerepo=lustre-server install \
lustre-dkms \
lustre-osd-zfs-mount \
lustre \
lustre-resource-agents \
zfs
```

Natomiast pojawił się problem brakującej paczki `lustre-resource-agents` którą ostatecznie pominięto. Oczekiwano kiedyś na błąd brakującej zależności w postaci tej paczki. Ale tak się nie stało.


```sh
Error: 
 Problem: conflicting requests
  - nothing provides resource-agents needed by lustre-resource-agents-2.15.3-1.el8.x86_64
(try to add '--skip-broken' to skip uninstallable packages or '--nobest' to use not only best candidate packages)
[root@KomputerA vagrant]# 
```

Ze względu na niskopoziomowe działanie lustre, wyróżnia się jeszcze zmodyfikowane przez lustre jądro systemu linux. Dlatego podczas instalacji powstanie kolejne, co pokazano poniżej.

```sh
[root@HostD vagrant]# uname -r
4.18.0-477.27.1.el8_8.x86_64
[root@HostD vagrant]# grubby --default-kernel
/boot/vmlinuz-4.18.0-477.10.1.el8_lustre.x86_64
[root@HostD vagrant]#
```

Można zauważyć że jądro lustre jest starsze (ale takie same co do wersji jak te przed aktualizacją). Co gorsza pojawił się problem z modułami. Lustre nie dostarczyło modułów jądra dla tan nowego zmodyfikowanego jądra. W efekcie występują 3 jądra, starsze, nowsze oraz zmodyfikowane z lustre bazujące na starszym.


Patrząc na sekcję z instrukcji `Lustre Servers with Both LDISKFS and ZFS OSD Support` można zauważyć że do instalacji paczek jądra została podana poniższa instrukcja.

```sh
yum --nogpgcheck --disablerepo=base,extras,updates \
--enablerepo=lustre-server install \
kernel \
kernel-devel \
kernel-headers \
kernel-tools \
kernel-tools-libs \
kernel-tools-libs-devel
```

Co więcej aby nie dopuścić do posiadania 3 jąder systemu przeszukano repozytorium `lustre-server` w poszukiwaniu konkretnej wersji paczki `kernel` i określono wersję
`VER="4.18.0-477.10.1.el8_lustre"`. Wersja ta jest wersją pochodząca z jądra systemu z repozytorium `baseos` ale zmodyfikowaną o lustre. Dlatego ważnym było dobranie odpowiedniej wersji

Chcąc wykonać poniższą komendę, napotkano problem braku paczek.

```sh
yum --nogpgcheck --disablerepo=base,extras,updates \
--enablerepo=lustre-server install \
kernel-$VER \
kernel-devel-$VER \
kernel-headers-$VER \
kernel-abi-whitelists-$VER \
kernel-tools-$VER \
kernel-tools-libs-$VER \
kernel-tools-libs-devel-$VER
```

Po przeszukaniu repozytoriów oraz ich zależności (żeby nie zainstalować jądra z `baseos` - niezmodyfikowanego) przygotowano poniższe komendy.

```sh
dnf install -y kernel-tools-libs-devel perl-interpreter # z baseos wraz z dependencjami dla komendy poniżej
VER="4.18.0-477.10.1.el8_lustre"
dnf --disablerepo=baseos,extras --enablerepo=lustre-server install -y kernel-$VER kernel-devel-$VER kernel-headers-$VER kernel-tools kernel-tools-libs
```

Niektóre paczki nie miały takiego samego wersjonowania po przeszukaniu repozytorium, mimo iż instrukcja wskazywała omylnie że każda paczka ma wersję odpowiadającej wersji jądra.

Po wykonaniu tych kroków na nowej maszynie, w efekcie były już dwa jądra, z czego system domyślnie uruchamiał się z jądra zmodyfikowanego o lustre.

Na jądrze niezmodyfikowanym przez lustre (`4.18.0-477.10.1.el8_8.x86_64`) były moduły zfs oraz lustre, ale aplikacje lustre (userspace) oraz moduły mogą tylko działać ze zmodyfikowanym jądrem lustre (`4.18.0-477.10.1.el8_lustre.x86_64`)

```sh
[root@HostC vagrant]# find /lib/modules -name *osd*
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/lustre-osd-ldiskfs
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/lustre-osd-ldiskfs/fs/osd_ldiskfs.ko
[root@HostC vagrant]# find /lib/modules -name *zfs*
/lib/modules/4.18.0-477.10.1.el8_8.x86_64/extra/zfs.ko.xz
```

Przechodząc dalej na jądrze lustre stworzono zpool oraz skonfigurowano `MGS, MDT, OST` lustre. Uruchamiając MGS na podstawie linku [https://wiki.lustre.org/Creating_the_Lustre_Management_Service_(MGS)](https://wiki.lustre.org/Creating_the_Lustre_Management_Service_(MGS)), przy montowaniu plików `mount -t lustre` pojawiał się błąd o braku systemu plików `osd-zfs`. Co potwierdza poprzednie wyjście programu.

Okazuje się że paczki lustre oraz zfs zainstalowały się nie w jądrze zmodyfikowanym z lustre, tylko w podstawowym jadrze linux-a. Dlatego postanowiono od nowa stworzyć maszynę, jednakże zaraz po uruchomieniu systemu z nowym jądrem lustre, usunięto oryginalne jądro, aby wymusić poprawną instalację. Poniżej pokazano usunięte paczki (a w nim jądro systemu)

```sh
[root@HostA vagrant]# dnf list installed | grep kernel | grep 4.18.0-477.10.1.el8_8
kernel.x86_64                         4.18.0-477.10.1.el8_8                              @anaconda
kernel-core.x86_64                    4.18.0-477.10.1.el8_8                              @anaconda
kernel-modules.x86_64                 4.18.0-477.10.1.el8_8                              @anaconda
```

Następnie, tak samo jak poprzednio zainstalowano paczki lustre oraz zfs, aczkolwiek pojawiły się poniższe ostrzezenia

```sh
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/


ym(zap_cursor_retrieve) = 0xb6b74205 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_cursor_serialize) = 0x69ee64fb is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_lookup) = 0xbca1cdd3 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_lookup_by_dnode) = 0xdf56a829 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_lookup_uint64) = 0x5cd32a34 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_remove) = 0x640897f8 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_remove_by_dnode) = 0x138a7f6f is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_remove_uint64) = 0x4f71f697 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zap_update) = 0x78182545 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zfs_attr_table) = 0xfbf319ac is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zfs_prop_to_name) = 0x5adabedf is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zio_buf_alloc) = 0x0c211976 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zio_buf_free) = 0x6e7a7166 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zrl_add_impl) = 0x23678d86 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
        ksym(zrl_remove) = 0x707c4b64 is needed by kmod-lustre-osd-zfs-2.15.3-1.el8.x86_64
```

Przeszukując moduły systemu znajdowały się one poprawnie w jednym jądrze.

```sh
modprobe -v lustre
modprobe -v zfs
```

Powyższe polecenie `lustre` zakończyło się pomyślnie, natomiast w przypadku `zfs` ostrzeżenia przy instalacji przekształciły się w błąd uniemożliwiający załadowanie modułu jądra.

Dlatego podjęto próby kompilacji zfs ze źródeł aby uspójnić różniącą się konfigurację [https://www.reddit.com/r/zfs/comments/ri1ly2/zfs_ksyms_and_lustre/](https://www.reddit.com/r/zfs/comments/ri1ly2/zfs_ksyms_and_lustre/)

Natomiast liczne błędy spowodowały zaniechania realizacji pomysłu.

Biorąc pod uwagę fakt że nowe jądro jest kompilowane na podstawie m.in poprzedniego, podjęto próbę rozwiązania problemu inaczej wskazano w instrukcji.

Najpierw zainstalowano część paczek lustre oraz zfs.

```sh
dnf install -y zfs lustre-osd-zfs-mount

dnf install -y kernel-tools-libs-devel perl-interpreter
dnf --nogpgcheck --disablerepo=* --enablerepo=e2fsprogs-wc install -y e2fsprogs
```

Po czym przy procesie budowania nowego jądra zmodyfikowanego z lustre (ok 30 min) wykorzystano poniższą komendę.

```sh
VER="4.18.0-477.10.1.el8_lustre"
dnf --disablerepo=baseos,extras --enablerepo=lustre-server install -y kernel-$VER kernel-devel-$VER kernel-headers-$VER kernel-tools kernel-tools-libs
```

Te autorskie rozwiązanie pozwoliło mieć dwa jądra. Na jądrze z lustre pomyślnie załadowano paczki oraz moduły zarówno lustre oraz zfs.
A ponadto system poprawnie rozpoznaje brakujacy system plików. Poniżej pokazano że interesujące moduły znajdują się w odpowiednim katalogu odpowiednim dla jądra

```sh
[root@HostB vagrant]# !15
find /lib/modules/ -name "*osd*"
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/osd_zfs.ko.xz
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/osd_ldiskfs.ko.xz
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/lustre-osd-ldiskfs
/lib/modules/4.18.0-477.10.1.el8_lustre.x86_64/extra/lustre-osd-ldiskfs/fs/osd_ldiskfs.ko
```

Podczas udręki z konfiguracją, oraz konfiguracji systemu w ilości kilkadziesiąt, dodatkowym problem były znaczące spadki przy pobieraniu paczek z serwerów udostępniających technologię lustre

```sh
(213/215): zfs-dkms-2.1.13-1.el8.noarch.rpm                                                                                                              1.2 MB/s |  30 MB     00:25     
(214/215): lustre-all-dkms-2.15.3-1.el8.noarch.rpm                                                                                                       307 kB/s |  20 MB     01:08     
[MIRROR] kernel-debuginfo-common-x86_64-4.18.0-477.10.1.el8_lustre.x86_64.rpm: Curl error (18): Transferred a partial file for https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/server/RPMS/x86_64/kernel-debuginfo-common-x86_64-4.18.0-477.10.1.el8_lustre.x86_64.rpm [transfer closed with 74402100 bytes remaining to read]
[MIRROR] kernel-debuginfo-common-x86_64-4.18.0-477.10.1.el8_lustre.x86_64.rpm: Curl error (18): Transferred a partial file for https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/server/RPMS/x86_64/kernel-debuginfo-common-x86_64-4.18.0-477.10.1.el8_lustre.x86_64.rpm [transfer closed with 75041076 bytes remaining to read]
(215/215): kernel-debuginfo-common-x86_64-4.18.0-477.10.1.el8_lustre.x86_ 70% [==================================================                      ]  70 kB/s | 171 MB     17:48 ETA 
```

## Diagram uruchamianych skryptów na maszynach

```mermaid
flowchart TD

1["stage1.sh
serwer"]

1c["stage1_client.sh
klient"]

2["stage2.sh
klient + serwer"]

3["stage3.sh
serwer"]

3c["stage3_client.sh
klient"]

4c["stage4_client.sh
klient"]

4ost["stage4_ost.sh
OST"]

4mgsmdt["stage4_mgs_mdt.sh
MGS MDT"]


1 -->  2
1c --> 2

2 --> 3
2 --> 3c

3c --> 4c
3 ---> 4ost
3 ---> 4mgsmdt

```

Poniżej przedstawiono przeznaczenie plików:

- W plikach `stage1` konfigurowane są repozytoria oraz zależności lustre oraz zfs.
- Plik `stage2` buduje zmodyfikowane jądro przez lustre.
- Pliki `stage3` to konfiguracja sieci, ładowanie modułów jądra oraz id hosta
- Pliki `stage4` to montowanie zasobów w zależności od przeznaczenia maszyny

Maszyna serwerowa ma więcej oprogramowania w porównaniu do klienckiej.

## Montowanie zasobów

Do komputerów serwerowych dołączono dwa dyski `sdb, sdc`.

Na komputerze pełniącego rolę `MGS` oraz `MDT` przeznaczono po jednym dysku na dane `mgs` oraz `mdt` Konfigurując je poniższymi komendami [stage4_mgs_mdt.sh](./stage4_mgs_mdt.sh)

Dla dwóch komputerów w roli `OST` wykorzystano jeden dysk jako zasów `ost` Konfigurując je poniższymi komendami [stage4_ost.sh](./stage4_ost.sh)

Całość jest dostępna dla komputera klienta.
