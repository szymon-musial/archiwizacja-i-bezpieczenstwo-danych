# Lustre nie wspiera selinuxa
sed --in-place "s/SELINUX=*/SELINUX=disabled/" /etc/selinux/config

dnf repolist

# repozytoria Lustre
cat >/etc/yum.repos.d/lustre.repo <<__EOF
[lustre-server]
name=lustre-server
baseurl=https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/server
# exclude=*debuginfo*
gpgcheck=0
enabled=1

[lustre-client]
name=lustre-client
baseurl=https://downloads.whamcloud.com/public/lustre/lustre-2.15.3/el8.8/client
# exclude=*debuginfo*
gpgcheck=0
enabled=1

[e2fsprogs-wc]
name=e2fsprogs-wc
baseurl=https://downloads.whamcloud.com/public/e2fsprogs/latest/el8
# exclude=*debuginfo*
gpgcheck=0
enabled=1
__EOF

# Niestabilne repo feodry - epel dla rocky linux-a
dnf -y install epel-release
# dnf -y install https://zfsonlinux.org/epel/zfs-release-2-3.el8.noarch.rpm

# Paczka dla kernela: kernel-tools-libs-devel 
sed --in-place "s/enabled=0/enabled=1/" /etc/yum.repos.d/Rocky-PowerTools.repo
grep "enabled=1" /etc/yum.repos.d/Rocky-PowerTools.repo

dnf repolist
dnf makecache

dnf install -y htop

# Niezgodna dokumentacja

dnf install -y lustre-client-dkms lustre-client

dnf install -y kernel-tools-libs-devel perl-interpreter
#dnf --nogpgcheck --disablerepo=* --enablerepo=e2fsprogs-wc install -y e2fsprogs

# do tej pory jest jedno jądro
