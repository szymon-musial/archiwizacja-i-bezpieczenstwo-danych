# Nowe jajko  
# package lustre-all-dkms-2.15.3-1.el8.noarch conflicts with lustre-client-dkms provided by lustre-client-dkms-2.15.3-1.el8.noarch

echo "options lnet networks=tcp(eth1)" > /etc/modprobe.d/lustre.conf

hid=`[ -f /etc/hostid ] && od -An -tx /etc/hostid|sed 's/ //g'`
echo $hid
[ "$hid" = `hostid` ] || genhostid

modprobe -v lustre

journalctl | tail -n 100
ss -tunlp | grep 988
