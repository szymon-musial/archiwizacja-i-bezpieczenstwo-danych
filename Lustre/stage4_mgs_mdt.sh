zpool create -O canmount=off -O mountpoint=none -o cachefile=none -o failmode=panic mgspool /dev/sdb
zpool create -O canmount=off -O mountpoint=none -o cachefile=none -o failmode=panic mdtpool /dev/sdc

zpool status

mkfs.lustre --reformat --backfstype=zfs --mgs --fsname=lstore --mgsnode=10.100.0.11@tcp mgspool/sdb
mkfs.lustre --reformat --backfstype=zfs --mdt --fsname=lstore --mgsnode=10.100.0.11@tcp mdtpool/sdc

zfs list
mkdir -p /lustre/mgs
mount -t lustre mgspool/sdb /lustre/mgs
# https://wiki.lustre.org/Creating_the_Lustre_Management_Service_(MGS)  


# checks 

journalctl | tail -n 100
df
lctl dl
ps -ef | awk '/mgs/ && !/awk/'
ss -tunlp | grep 988

lctl ping 10.100.0.11@tcp # ping mgs