# must be unique
[[ -z "$1" ]] && { echo "OSD Index is empty" ; exit 1; }
index=$1
echo "Index $index"

zpool create -O canmount=off -O mountpoint=none -o cachefile=none -o failmode=panic ostpool /dev/sdb
mkfs.lustre --reformat --backfstype=zfs --ost --fsname=lstore --mgsnode=10.100.0.11@tcp --index=$index ostpool/sdb

lsblk

zfs list
mkdir -p /lustre/ost
mount -t lustre ostpool/sdb /lustre/ost

# checks 

journalctl | tail -n 100
df
lctl dl
ps -ef | awk '/mgs/ && !/awk/'
ss -tunlp | grep 988

lctl ping 10.100.0.11@tcp # ping mgs