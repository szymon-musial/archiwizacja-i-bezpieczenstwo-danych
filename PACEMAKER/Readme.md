# PACEMAKER

Zadanie polegało na utworzeniu klastra o wysokiej dostępności który powinien w razie awarii jednego z serwerów apache2 wykryć występujący problem oraz przekierować ruch do sprawnego węzła.

W tym ćwiczeniu zostaną użyte takie programy jak:

- `corosync` - który odpowiada za komunikacje pomiędzy węzłami klastra
- `pacemaker` - który odpowiada za zarządzanie klastrem (tworzenie nowego klastra, sprawdzanie stanu połączeń, definiowaniu zasobów itd...)

### Przygotowanie do ćwiczenia

Całość ćwiczenia została zrealizowana wykorzystując środowisko `vagrant`, a jako system operacyjny zainstalowany na maszynie wirtualnej został użyty system operacyjny `fedora 35`. W plikach startujących obrazy zostały zainstalowane wszystkie wymagane zależności.

Pierwszym krokiem było wyłączenie `SELinuxa`. Można tego dokonać edutując konfiguracje w pliku `/etc/selinux/config`. Powinna zostać wstawiona taka wartość jak na poniższym fragmencie kodu.

```
SELINUX=disabled
```

Kolejnym krokiem jest zdefiniowanie hostów zarówno w jednym jak i w drugim hoście. Plik `/etc/hosts` ma taką zawartość jak widać poniżej.

```
192.168.1.201 pcmk-1.clusterlabs.org pcmk-1
192.168.1.202 pcmk-2.clusterlabs.org pcmk-2
```

Możemy również hostą nadać odpowiednie hostname

```
hostname pmck-(1|2)
```

### Konfiguracja corosync

Konfigurując `corosync` musimy podmienić konfiguracje w kilku plikach. Pierwszym z nich jest `/etc/cluser/cluster.conf`

```
totem {
      version: 2
      cluster_name: ExampleCluster
      crypto_cipher: none
      crypto_hash: none
      interface {
            ringnumer: 0
            bindnetaddr: 192.168.1.0
            broadcast: yes
      }
}
logging {
      fileline: off
      to_stderr: yes
      to_logfile: yes
      logfile: /var/log/cluster/corosync.log
      to_syslog: yes
      debug: off
      logger_subsys {
            subsys: QUORUM
            debug: off
      }
}
quorum {
      provider: corosync_votequorum
}
nodelist {
      node {
            name: pcmk-1
            nodeid: 1
            ring0_addr: pcmk-1
      }
      node {
            name: pcmk-2
            nodeid: 2
            ring0_addr: pcmk-2
      }
}
```

Kolejnym z nich jest `/etc/corosync/service.d/pcmk`

```
service {
 name: pacemaker
 ver: 1
}
```

Ostatnim `/etc/default/corosync`

```
START=yes
```

Musimy również utworzyć klucz i rozpropagować go na maszynach.

```
sudo corosync-keygen
sudo spc /etc/corosync/authkey vagrant@pcmk-2:/tmp
```

Na drugiej maszynie:

```
sudo mv /tmp/authkey /etc/corosync
sudo chown root: /etc/corosync/authkey
sudo chmod 400 /etc/corosync/authkey
```

Na koniec uruchamiamy serwis. Skonfigurować musimy obie maszyny.

```
sudo systemctl corosync start
```

Na tym etapie możemy zweryfikowac poprawność działania `corosync`

```
[vagrant@pcmk-2 ~]$ sudo corosync-cmapctl | grep members
runtime.members.1.config_version (u64) = 0
runtime.members.1.ip (str) = r(0) ip(192.168.1.201)
runtime.members.1.join_count (u32) = 1
runtime.members.1.status (str) = joined
runtime.members.2.config_version (u64) = 0
runtime.members.2.ip (str) = r(0) ip(192.168.1.202)
runtime.members.2.join_count (u32) = 1
runtime.members.2.status (str) = joined
```

### Konfiguracja pacemaker i apache

By uruchomić klaster musimy wywołać komendy

```
sudo pcs cluster auth pcmk-1 pcmk-2
pcs cluster start
```

Teraz możemy sprawdzić poprawność działania klastra

```
[vagrant@pcmk-2 ~]$ sudo pcs status
Cluster name: SWDCluster
Cluster Summary:
  * Stack: corosync
  * Current DC: pcmk-1 (version 2.1.4-4.fc35-dc6eb4362e) - partition with quorum
  * Last updated: Sun Oct 29 19:22:30 2023
  * Last change:  Sun Oct 29 14:49:14 2023 by hacluster via crmd on pcmk-1
  * 2 nodes configured
  * 2 resource instances configured

Node List:
  * Online: [ pcmk-1 pcmk-2 ]

Full List of Resources:
  * No resources

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled
```

Ustawiamy parametry klastara

```
sudo pcs property set stonith-enabled=false
sudo pcs property set no-quorum-policy=ignore
sudo pcs resource defaults migration-treshold=1
```

Instalujemy serwer apache

```
sudo dnf install httpd
```

Pliki strony internetowej znajdują się w katalogu `/var/www/html`. Możemy tam dodać kod przykładowej strony internetowej.

```
<h1>Hello from pcmk=(1|2) </h1>
```

Musimy utworzyć plik `/etc/httpd/conf.d/status.conf` z poniższą zawartością. Potrzebujemy tego by móc odpytywać o aktualny status serwera www.

```
 <Location /server-status>
    SetHandler server-status
    Require local
 </Location>
```

Na koniec pozostało nam uruchomić serwis.

```
sudo systemctl start httpd
```

Teraz musimy jeszcze skonfigurować zasoby klastra.

```
sudo pcs resource create virtual_ip ocf:heartbeat:IPaddr2 ip=192.168.1.200 cid_netmask=24 op monitor interval=1min
sudo pcs resource create WebSite ocf:heartbeat:apache configfile=/etc/httpd/conf/httpd.conf statusurl="http://localhost/server-status" op monitor interval=1min
sudo pcs constraint colocation add WebSite with wirtual_ip INFINITY
sudo pcs constraint order virtual_ip then WebSite
```

Warto sprawdzić czy w pliku `/etc/httpd/conf/httpd.conf` widnieje wpis `PidFile /var/run/httpd/httpd.pid`. Bez tego wpisu mogą występować błędy.

Na koniec możemy jeszcze raz sprawdzić status klastra

```
[vagrant@pcmk-2 ~]$ sudo pcs status
Cluster name: SWDCluster
Cluster Summary:
  * Stack: corosync
  * Current DC: pcmk-1 (version 2.1.4-4.fc35-dc6eb4362e) - partition with quorum
  * Last updated: Sun Oct 29 19:37:26 2023
  * Last change:  Sun Oct 29 14:49:14 2023 by hacluster via crmd on pcmk-1
  * 2 nodes configured
  * 2 resource instances configured

Node List:
  * Online: [ pcmk-1 pcmk-2 ]

Full List of Resources:
  * virtual_ip  (ocf::heartbeat:IPaddr2):        Started pcmk-1
  * Resource Group: apache:
    * WebSite   (ocf::heartbeat:apache):         Started pcmk-1

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled
```

### Testy

Na samym początku warto sprawdzić działanie serwerów www podając bezpośredni adres ip hosta a następnie podając adres wirtualny. Powinniśmy być wstasnie otrzymać stronę na wszystkie sposoby. Możemy teraz wyłączyć serwer http na hoscie `pcmk-1` (`sudo systemclt stop httpd`). Sprawdzając status klastra powinniśmy zobaczyć że zasoby pobierane są teraz z węzła `pcmk-2` (może zająć kilka minut).
