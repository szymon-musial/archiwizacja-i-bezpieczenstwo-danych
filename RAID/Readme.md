# 8. Macierze RAID

### Testy pojedyńczych dysków

Wylistowane wszystkie dostępne dyski

```
user@Debian:~$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda      8:0    0   20G  0 disk
├─sda1   8:1    0   19G  0 part /
├─sda2   8:2    0    1K  0 part
└─sda5   8:5    0  975M  0 part [SWAP]
sdb      8:16   0    1G  0 disk
sdc      8:32   0    1G  0 disk
sdd      8:48   0    1G  0 disk
sde      8:64   0    1G  0 disk
sdf      8:80   0    1G  0 disk
sdg      8:96   0    1G  0 disk
sr0     11:0    1 50.6M  0 rom

```

| Nr. Dysku | Plik urządzenia | Odczyt (hdparm) [MB/s] | Zapis (dd) [MB/s] |
| --------- | --------------- | ---------------------- | ----------------- |
| 1         | /dev/sdb        | 306                    | 138               |
| 2         | /dev/sdc        | 330                    | 144               |
| 3         | /dev/sdd        | 310                    | 118               |
| 4         | /dev/sde        | 315                    | 137               |
| 5         | /dev/sdf        | 276                    | 128               |
| 6         | /dev/sdg        | 299                    | 124               |
| 7         | /dev/sdh        | 247                    | 95                |

### RAID 0

`sudo mdadm --create --verbose /dev/md0 --raid-devices=n --level=0 /dev/sdb /dev/sdc ....`

Wylistowane dyski po utworzeniu macierzy RAID 0

```
user@Debian:~$ lsblk
NAME      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0   20G  0 disk
├─sda1      8:1    0   19G  0 part  /
├─sda2      8:2    0    1K  0 part
└─sda5      8:5    0  975M  0 part  [SWAP]
sdb         8:16   0    1G  0 disk
└─md0       9:0    0    4G  0 raid0
  └─md0p1 259:0    0    4G  0 part
sdc         8:32   0    1G  0 disk
└─md0       9:0    0    4G  0 raid0
  └─md0p1 259:0    0    4G  0 part
sdd         8:48   0    1G  0 disk
└─md0       9:0    0    4G  0 raid0
  └─md0p1 259:0    0    4G  0 part
sde         8:64   0    1G  0 disk
└─md0       9:0    0    4G  0 raid0
  └─md0p1 259:0    0    4G  0 part
sdf         8:80   0    1G  0 disk
sdg         8:96   0    1G  0 disk
sr0        11:0    1 50.6M  0 rom
```

| Ilość dysków | Odczyt (hdparm) [MB/s] | Zapis (dd) [MB/s] |
| ------------ | ---------------------- | ----------------- |
| 2            | 353                    | 147               |
| 3            | 316                    | 136               |
| 4            | 329                    | 155               |

### RAID 1

`sudo mdadm --create --verbose /dev/md0 --raid-devices=n --level=1 /dev/sdb /dev/sdc ....`

Wylistowane dyski po utworzeniu macierzy RAID 1

```
user@Debian:~$ lsblk
NAME      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0   20G  0 disk
├─sda1      8:1    0   19G  0 part  /
├─sda2      8:2    0    1K  0 part
└─sda5      8:5    0  975M  0 part  [SWAP]
sdb         8:16   0    1G  0 disk
└─md0       9:0    0 1022M  0 raid1
  └─md0p1 259:0    0 1020M  0 part
sdc         8:32   0    1G  0 disk
└─md0       9:0    0 1022M  0 raid1
  └─md0p1 259:0    0 1020M  0 part
sdd         8:48   0    1G  0 disk
└─md0       9:0    0 1022M  0 raid1
  └─md0p1 259:0    0 1020M  0 part
sde         8:64   0    1G  0 disk
└─md0       9:0    0 1022M  0 raid1
  └─md0p1 259:0    0 1020M  0 part
sdf         8:80   0    1G  0 disk
sdg         8:96   0    1G  0 disk
sr0        11:0    1 50.6M  0 rom
```

| Ilość dysków | Odczyt (hdparm) [MB/s] | Zapis (dd) [MB/s] |
| ------------ | ---------------------- | ----------------- |
| 2            | 307                    | 96                |
| 3            | 307                    | 72                |
| 4            | 303                    | 44                |

### RAID 5

`sudo mdadm --create --verbose /dev/md0 --raid-devices=n --level=5 /dev/sdb /dev/sdc ....`

Wylistowane dyski po utworzeniu macierzy RAID 5

```
user@Debian:~$ lsblk
NAME      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0   20G  0 disk
├─sda1      8:1    0   19G  0 part  /
├─sda2      8:2    0    1K  0 part
└─sda5      8:5    0  975M  0 part  [SWAP]
sdb         8:16   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdc         8:32   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdd         8:48   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sde         8:64   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdg         8:80   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdf         8:96   0    1G  0 disk
sr0        11:0    1 50.6M  0 rom
```

| Ilość dysków | Odczyt (hdparm) [MB/s] | Zapis (dd) [MB/s] |
| ------------ | ---------------------- | ----------------- |
| 3            | 308                    | 98                |
| 4            | 268                    | 75                |
| 5            | 307                    | 112               |

## Test RAID 5

Pierwszym krokiem jest stworzenie partycji za pomocą programu `fdisk`. Następnie musimy stworzoną partycje sformatować i zmontować.

```
sudo mkfs -t ext4 /dev/md0p1
sudo mount /dev/md0p1 disk
```

Tworzymy plik i dodajemy do niego przykładowy tekst `RAID 5 TEST`.

Wykonujemy komendę `sudo mdadm --manage /dev/md0 --fail /dev/sdg` dzięki czemu oznaczamy dysk jako ten na którym wystąpiła awaria.

Możemy teraz podejżeć detale macierzy

```
user@Debian:~$ sudo mdadm --detail /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Sat Oct  7 11:31:37 2023
        Raid Level : raid5
        Array Size : 4186112 (3.99 GiB 4.29 GB)
     Used Dev Size : 1046528 (1022.00 MiB 1071.64 MB)
      Raid Devices : 5
     Total Devices : 5
       Persistence : Superblock is persistent

       Update Time : Sat Oct  7 11:54:52 2023
             State : clean, degraded, resyncing (PENDING)
    Active Devices : 4
   Working Devices : 4
    Failed Devices : 1
     Spare Devices : 0

            Layout : left-symmetric
        Chunk Size : 512K

Consistency Policy : resync

              Name : Debian:0  (local to host Debian)
              UUID : 07ee3904:0360745d:be522cd3:eab9a5f0
            Events : 22

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync   /dev/sdb
       1       8       32        1      active sync   /dev/sdc
       2       8       48        2      active sync   /dev/sdd
       3       8       64        3      active sync   /dev/sde
       -       0        0        4      removed

       5       8       96        -      faulty   /dev/sdg

```

Mimo iż jeden dysk jest uszkodzony w dalszym stopniu mamy dostęp do danych. By odbudować macierz musimy usunąć dysk z awarią `sudo mdadm --manage /dev/md0 --remove /dev/sdg` a następnie dodać nowy `sudo mdadm --manage /dev/md0 --add /dev/sdf`. W tym momencie posiadamy już odbudowaną macierz RAID 5.

```
user@Debian:~$ lsblk
NAME      MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
sda         8:0    0   20G  0 disk
├─sda1      8:1    0   19G  0 part  /
├─sda2      8:2    0    1K  0 part
└─sda5      8:5    0  975M  0 part  [SWAP]
sdb         8:16   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdc         8:32   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdd         8:48   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sde         8:64   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdf         8:80   0    1G  0 disk
└─md0       9:0    0    4G  0 raid5
  └─md0p1 259:0    0    4G  0 part
sdg         8:96   0    1G  0 disk
sr0        11:0    1 50.6M  0 rom
```

Na dysku dalej znajduje się utworzony przez nas plik.

```
user@Debian:~/disk$ ls
test.txt
user@Debian:~/disk$ cat test.txt
RAID 5 TEST
user@Debian:~/disk$
```
