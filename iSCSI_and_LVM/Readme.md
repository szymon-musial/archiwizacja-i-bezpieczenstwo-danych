# iSCSI z LVM

[Instrukcja](http://www.math.us.edu.pl/~pgladki/teaching/2017-2018/abd/iSCSI-v2.pdf)

Paczki z instrukcji są przestarzałe

## References:

- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/managing_storage_devices/configuring-an-iscsi-target_managing-storage-devices#doc-wrapper
- 

## iSCSI

### pscsi 

```log
Oct 05 19:18:55 KomputerB.lab7 kernel: I/O error, dev sdc, sector 69896 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 2
Oct 05 19:18:55 KomputerB.lab7 kernel: EXT4-fs error (device sdc): __ext4_find_entry:1664: inode #2: comm bash: reading directory lblock 0
Oct 05 19:18:55 KomputerB.lab7 kernel: I/O error, dev sdc, sector 0 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 2
Oct 05 19:18:55 KomputerB.lab7 kernel: Buffer I/O error on dev sdc, logical block 0, lost sync page write
Oct 05 19:18:55 KomputerB.lab7 kernel: EXT4-fs (sdc): I/O error while writing superblock
```

### Tworzenie i udostępnianie urządzenie przy pomocy iSCSI

Poza vagrantem dodano dyski do maszyn

[root@KomputerA vagrant]# lsscsi | grep sdb
[2:0:1:0]    disk    VMware,  VMware Virtual S 1.0   /dev/sdb


1. Nazwy zasobów iSCSI
    - Komputer A: iqn.2003-01.komputera.lab7:444
    - Komputer C: iqn.2003-01.komputerc.lab7:111

### Konfiguracja serwerów udostępniających zasoby przez protokół iSCSI

a) [plik](./cfg)


b)

```sh
[root@KomputerA vagrant]# systemctl status target
● target.service - Restore LIO kernel target configuration
     Loaded: loaded (/usr/lib/systemd/system/target.service; enabled; preset: disabled)
     Active: active (exited) since Thu 2023-10-05 20:13:17 UTC; 8s ago
   Main PID: 5014 (code=exited, status=0/SUCCESS)
        CPU: 94ms

Oct 05 20:13:17 KomputerA.lab7 systemd[1]: Starting Restore LIO kernel target configuration...
Oct 05 20:13:17 KomputerA.lab7 target[5014]: No saved config file at /etc/target/saveconfig.json, ok, exiting
Oct 05 20:13:17 KomputerA.lab7 systemd[1]: Finished Restore LIO kernel target configuration.
```
c) 
```sh
targetctl restore /tmp/new_cfg 
```

d)

```sh
firewall-cmd --permanent --add-port=3260/tcp
```


### Konfiguracja klienta zbierającego udostępnione zasoby protokołem iSCSI

a) b) c)

```sh
for ip in 10.100.0.1{1,3};
do
    portal="`iscsiadm -m discovery -t st -p $ip | cut -d ' ' -f 2`"
    echo $portal
    iscsiadm -m node -T $portal -l
done
```

```sh
iqn.2003-01.komputera.lab7:111
Logging in to [iface: default, target: iqn.2003-01.komputera.lab7:111, portal: 10.100.0.11,3260]
Login to [iface: default, target: iqn.2003-01.komputera.lab7:111, portal: 10.100.0.11,3260] successful.
iqn.2003-01.komputerc.lab7:111
Logging in to [iface: default, target: iqn.2003-01.komputerc.lab7:111, portal: 10.100.0.13,3260]
Login to [iface: default, target: iqn.2003-01.komputerc.lab7:111, portal: 10.100.0.13,3260] successful.
```



```sh
[root@KomputerB vagrant]# dmesg | tail -n 20
[  756.593102] sd 3:0:0:0: alua: port group 00 state A non-preferred supports TOlUSNA
[  756.594450] sd 3:0:0:0: [sdb] 8388608 512-byte logical blocks: (4.29 GB/4.00 GiB)
[  756.597584] sd 3:0:0:0: [sdb] Write Protect is off
[  756.598276] sd 3:0:0:0: [sdb] Mode Sense: 43 00 00 08
[  756.602163] sd 3:0:0:0: [sdb] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[  756.609976] sd 3:0:0:0: [sdb] Preferred minimum I/O size 512 bytes
[  756.610852] sd 3:0:0:0: [sdb] Optimal transfer size 33550336 bytes
[  756.626116] sd 3:0:0:0: [sdb] Attached SCSI disk
[  757.676765] scsi host4: iSCSI Initiator over TCP/IP
[  757.693240] scsi 4:0:0:0: Direct-Access     LIO-ORG  block_backend    4.0  PQ: 0 ANSI: 6
[  757.708681] scsi 4:0:0:0: alua: supports implicit and explicit TPGS
[  757.709586] scsi 4:0:0:0: alua: device naa.60014054b991dd29e764cb7b3f6b12b9 port group 0 rel port 1
[  757.715329] sd 4:0:0:0: Attached scsi generic sg2 type 0
[  757.719078] sd 4:0:0:0: [sdc] 8388608 512-byte logical blocks: (4.29 GB/4.00 GiB)
[  757.723300] sd 4:0:0:0: [sdc] Write Protect is off
[  757.724296] sd 4:0:0:0: [sdc] Mode Sense: 43 00 00 08
[  757.727179] sd 4:0:0:0: [sdc] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[  757.732083] sd 4:0:0:0: [sdc] Preferred minimum I/O size 512 bytes
[  757.733285] sd 4:0:0:0: [sdc] Optimal transfer size 33550336 bytes
[  757.754903] sd 4:0:0:0: [sdc] Attached SCSI disk
```

```sh
[root@KomputerB vagrant]# cat /proc/partitions 
major minor  #blocks  name

   8        0   10485760 sda
   8        1     102400 sda1
   8        2    1024000 sda2
   8        3       4096 sda3
   8        4       1024 sda4
   8        5    8192000 sda5
   8       16    4194304 sdb
   8       32    4194304 sdc
```

```sh
[root@KomputerB vagrant]# cat /proc/scsi/scsi 
Attached devices:
Host: scsi2 Channel: 00 Id: 00 Lun: 00
  Vendor: VMware,  Model: VMware Virtual S Rev: 1.0
  Type:   Direct-Access                    ANSI  SCSI revision: 02
Host: scsi3 Channel: 00 Id: 00 Lun: 00
  Vendor: LIO-ORG  Model: block_backend    Rev: 4.0
  Type:   Direct-Access                    ANSI  SCSI revision: 06
Host: scsi4 Channel: 00 Id: 00 Lun: 00
  Vendor: LIO-ORG  Model: block_backend    Rev: 4.0
  Type:   Direct-Access                    ANSI  SCSI revision: 06
```


# LVM

1. 

```sh
[root@KomputerB vagrant]# pvs
  PV         VG Fmt  Attr PSize PFree
  /dev/sdb      lvm2 ---  4.00g 4.00g
  /dev/sdc      lvm2 ---  4.00g 4.00g
```

```sh
[root@KomputerB vagrant]# vgs
  VG #PV #LV #SN Attr   VSize  VFree
  VG   2   1   0 wz--n- <7.94g    0
```

```sh
[root@KomputerB vagrant]# lvs
  LV   VG Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv   VG -wi-a----- <7.94g
```

```sh
[root@KomputerB vagrant]# lsblk -b /dev/VG/LV
NAME  MAJ:MIN RM       SIZE RO TYPE MOUNTPOINTS
VG-LV 253:0    0 8522825728  0 lvm 
```


2. 

### hdparm

```sh
[root@KomputerB vagrant]# hdparm -Tt /dev/VG/LV

/dev/VG/LV:
 Timing cached reads:   24032 MB in  1.99 seconds = 12064.27 MB/sec
 Timing buffered disk reads: 508 MB in  3.08 seconds = 164.68 MB/sec
```

### dd

```sh
[root@KomputerB vagrant]# dd if=/dev/zero of=/dev/VG/LV bs=10MB count=10
10+0 records in
10+0 records out
100000000 bytes (100 MB, 95 MiB) copied, 0.512517 s, 195 MB/s
```

```sh
[root@KomputerB vagrant]# dd if=/dev/VG/LV of=/dev/null bs=10MB count=10
10+0 records in
10+0 records out
100000000 bytes (100 MB, 95 MiB) copied, 1.06116 s, 94.2 MB/s
```

3. 

```sh
mkfs.ext4 /dev/VG/LV
mkdir -p /mnt/iSCSI
mount /dev/VG/LV /mnt/iSCSI
```

4.

### hdparm 

```sh
[root@KomputerB iSCSI]#  hdparm -Tt /dev/VG/LV

/dev/VG/LV:
 Timing cached reads:   20368 MB in  1.99 seconds = 10220.58 MB/sec
 Timing buffered disk reads: 508 MB in  3.11 seconds = 163.36 MB/sec
```
### dd

```sh
[root@KomputerB iSCSI]# dd if=/dev/zero of=/mnt/iSCSI/test_file bs=10MB count=10
10+0 records in
10+0 records out
100000000 bytes (100 MB, 95 MiB) copied, 0.150842 s, 663 MB/s
```

```sh
[root@KomputerB iSCSI]# dd if=/mnt/iSCSI/test_file of=/dev/null bs=10MB count=10
10+0 records in
10+0 records out
100000000 bytes (100 MB, 95 MiB) copied, 0.0123986 s, 8.1 GB/s
```

### tar (without compression)

```sh
[root@KomputerB iSCSI]# time tar -cf ./tar /usr 2>/dev/null

real    0m3.927s
user    0m0.125s
sys     0m0.863s
```


## Net test

```sh

[root@KomputerB iSCSI]# iperf3  -c 10.100.0.11
Connecting to host 10.100.0.11, port 5201
[  5] local 10.100.0.12 port 43724 connected to 10.100.0.11 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec   397 MBytes  3.33 Gbits/sec  184   1.60 MBytes       
[  5]   1.00-2.00   sec   381 MBytes  3.20 Gbits/sec    0   1.73 MBytes       
[  5]   2.00-3.00   sec   338 MBytes  2.83 Gbits/sec    0   1.85 MBytes       
[  5]   3.00-4.00   sec   405 MBytes  3.40 Gbits/sec   17   1.48 MBytes       
[  5]   4.00-5.00   sec   420 MBytes  3.52 Gbits/sec    0   1.64 MBytes       
[  5]   5.00-6.00   sec   345 MBytes  2.89 Gbits/sec    0   1.77 MBytes       
[  5]   6.00-7.00   sec   355 MBytes  2.97 Gbits/sec   69   1.34 MBytes       
[  5]   7.00-8.00   sec   414 MBytes  3.48 Gbits/sec    0   1.53 MBytes       
[  5]   8.00-9.00   sec   372 MBytes  3.12 Gbits/sec    0   1.68 MBytes       
[  5]   9.00-10.00  sec   325 MBytes  2.73 Gbits/sec    0   1.80 MBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  3.66 GBytes  3.15 Gbits/sec  270             sender
[  5]   0.00-10.02  sec  3.66 GBytes  3.14 Gbits/sec                  receiver

iperf Done.

```