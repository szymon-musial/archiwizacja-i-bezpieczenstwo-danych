dnf install -y iscsi-initiator-utils

systemctl enable --now iscsid
systemctl status iscsid | head

# Fixed name
echo "InitiatorName=iqn.2003-01.komputerb.lab7:111" > /etc/iscsi/initiatorname.iscsi
systemctl restart iscsid

echo "Before logs"
grep "Attached SCSI" /var/log/messages
echo "Before lsblk"
lsblk

for ip in 10.100.0.1{1,3};
do
    portal="`iscsiadm -m discovery -t st -p $ip | cut -d ' ' -f 2`"
    echo $portal
    iscsiadm -m node -T $portal -l
done

echo "After logs"
grep "Attached SCSI" /var/log/messages
echo "After lsblk"
lsblk
