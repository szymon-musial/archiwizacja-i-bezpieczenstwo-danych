mkfs.ext4 /dev/sdb

dnf install -y targetcli

systemctl enable --now target
systemctl status target

systemctl enable --now firewalld.service 
firewall-cmd --permanent --add-port=3260/tcp
firewall-cmd --reload
firewall-cmd --list-all

cp /vagrant/cfg /tmp/cfg
sed  s/my_wwn/iqn.2003-01.$(hostname):111/ /tmp/cfg  > /tmp/new_cfg 
echo "Name"
grep wwn /tmp/new_cfg 

targetctl restore /tmp/new_cfg 