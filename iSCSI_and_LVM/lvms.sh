dnf install -y lvm2 hdparm

pvcreate PV /dev/sd{b,c}
pvs

vgcreate VG /dev/sd{b,c}
vgs

lvcreate -n LV -l 100%FREE VG
lvs

mkfs.ext4 /dev/VG/LV
mkdir -p /mnt/iSCSI
mount /dev/VG/LV /mnt/iSCSI